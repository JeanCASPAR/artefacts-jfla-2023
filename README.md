## Artefacts for JFLA 2024

# Compilation instructions
Run `make` to compile the whole project, `make cpp` to compile the C++ part or `make ocaml` to
compile the OCaml part. The C++ executable is called `btree.exe` and the ocaml executable is
called `main.exe`. You can run `make clean` to clean all files. You need `clang++`, `ocamlopt`
and `make` installed.

# OCaml
The OCaml files contain the interface and some example of code of types for which I
gave an efficient destructor by composing with other efficient destructors, in the most
mechanical way possible.

You can find explanations of each constructions in the `.mli` files. The file `drop_iter.ml` defines
the `Drop` and `DropIter` interfaces, as well as a `Base` interface allowing the user
to select the first value used by the continuation, and some other utilities.
`drop_iter_array.ml` contains a functor that provides an efficient destructor for
`t array`, when given an efficient destructor for `t`. `drop_iter_mutu_rec.ml` contains
an example of mutually recursive structures and their efficient destructors.You can
find the efficient destructor for the B-tree inside `drop_iter_btree.ml`, as well
as a nonregular datatypes for which our transformation works, but only when the functor for the
B-tree takes an efficient destructor for its parameter, because the destructor of `nonregular`
call the destructor of `nonregular btree` and vice versa. Finally, the `main.ml` file
contains some example code that will build some values and then destruct them, printing
the numbers destructed in order.

# btree.cpp & btree.hpp
This is a demonstration of the unboxed interface. It will first create
a B-tree initialized to 0 which will generate a lot of "Drop 0", then it will destroy
after printing "Start dropping". You can see that it is dropped in order, without allocation.
The `CPSPointer` is a fat pointer representing a pointer and a tag. It comes with some basic
utilities. The B-tree is simplified: it only holds one vector of children and a data that needs
to be dropped, represented here by a `std::unique_pointer<int>`.