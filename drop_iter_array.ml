open Drop_iter;;

(* The user must count the number of states of D *)
module DropIterArray (Self: Base) (D : DropIter) : DropIter with
  type t = D.t array
= struct
  type t = D.t array
  type cps =
  | Aux0 of unit (* unit represents here an unknown cps, whose type we don't know *)
  | Aux1 of unit * int
  let nb_state () = 2
  let base () = Self.base
  let drop_iter x k = if Array.length x = 0
    then D.invoke k
    else begin
      let y = x.(0) in
      x.(0) <- Obj.magic k;
      Obj.set_tag (Obj.repr x) (base ());
      D.drop_iter y (Obj.magic x)
    end

  let rec drop_iter' n x k = if Array.length x = n
      then D.invoke k
      else begin
        let repr = Sys.opaque_identity x |> Obj.repr in
        let y = x.(n) in
        Obj.set_tag repr (base () + 1);
        x.(1) <- Obj.magic (n + 1);
        D.drop_iter y (Obj.obj repr)
      end
  and invoke k =
    let repr = Sys.opaque_identity k |> Obj.repr in
    let x = Obj.obj repr in
    let tag = Obj.tag repr in
    if Obj.is_int repr
      then ()
    else if not (base () <= tag && tag < base () + nb_state ())
      then D.invoke k
    else begin
      Obj.set_tag repr (tag - base ());
      match Obj.obj repr with
      | Aux0 k' -> drop_iter' 1 x k'
      | Aux1 (k', n) -> drop_iter' n x k'
    end
end
