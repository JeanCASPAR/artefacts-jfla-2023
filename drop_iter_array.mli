open Drop_iter;;

(* efficient destructor for the array, parameterized
   by the base value and the efficient destructor for 'a *)
module DropIterArray :
  functor (Self : Base) (D : DropIter)
    -> DropIter with type t = D.t array