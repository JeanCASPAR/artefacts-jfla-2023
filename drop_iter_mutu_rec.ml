open Drop_iter;;

type xtree =
| XLeaf
| XTree of ytree * int * ytree
and ytree =
| YLeaf
| YTreeX of xtree * int
| YTreeY of ytree * int

type cps =
| Empty
| KX of cps * int * ytree
| KY of cps * int

module rec DropIterXTree : DropIter with type t = xtree = struct
  type t = xtree
  let base () = 0
  let nb_state () = 2
  let rec drop_iter t k = match t with
  | XLeaf -> invoke k
  | XTree (y, _, _) ->
    let y = Sys.opaque_identity y in
    let t = Sys.opaque_identity t in
    let k = Sys.opaque_identity k in
    let repr = Obj.repr t in
    Obj.repr k |> Obj.set_field repr 0;
    Obj.set_tag repr 0;
    DropIterYTree.drop_iter y (Obj.obj repr)
  and invoke : type a . a -> unit = fun k -> match Obj.magic k with
  | Empty -> ()
  | KX (k, n, y) -> Format.printf "%d@." n; DropIterYTree.drop_iter y k
  | KY (k, n) -> Format.printf "%d@." n; invoke k

end and DropIterYTree : DropIter with type t = ytree = struct
  type t = ytree
  let base () = 0
  let nb_state () = 2
  let rec drop_iter t k = match t with
  | YLeaf -> invoke k
  | YTreeX (x, _) ->
    let x = Sys.opaque_identity x in
    let t = Sys.opaque_identity t in
    let k = Sys.opaque_identity k in
    let repr = Obj.repr t in
    Obj.repr k |> Obj.set_field repr 0;
    Obj.set_tag repr 2;
    DropIterXTree.drop_iter x (Obj.obj repr)
  | YTreeY (y, _) ->
    let y = Sys.opaque_identity y in
    let t = Sys.opaque_identity t in
    let k = Sys.opaque_identity k in
    let repr = Obj.repr t in
    Obj.repr k |> Obj.set_field repr 0;
    Obj.set_tag repr 1;
    DropIterYTree.drop_iter y (Obj.obj repr)
  and invoke k = DropIterXTree.invoke k
end
