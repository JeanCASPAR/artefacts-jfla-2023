open Drop_iter;;
open Drop_iter_array;;

type key = int

type 'a pairs = (key * 'a) array

type 'a btree =
| Leaf of 'a pairs
| Node of 'a pairs * 'a btree array

(* DropIter for a product type, the right member one having a DropIter implementation *)
(* The user must handle the number of states of D *)
module RightProd(M : sig type t end) (D: DropIter) : DropIter
  with type t = M.t * D.t
= struct
  type t = M.t * D.t
  let nb_state () = 0
  let base () = 0
  let drop_iter (_, x) k = D.drop_iter x k
  let invoke k = D.invoke k
end

(* The user must handle the number of states of D *)
module DropIterBtree (Self: Base) (D: DropIter) : DropIter with
  type t = D.t btree
= struct
  module rec Impl : DropIter with
  type t = D.t btree
  = struct
    type t = D.t btree
    type cps = Empty | Aux of cps * D.t array
    let base () = Self.base

    module ArrayOfSelfBase = struct let base = Self.base + 1 end    
    module ArrayOfSelf : DropIter with type t = t array = DropIterArray(ArrayOfSelfBase)(Impl)
    
    (* we know it has no state *)
    module KeyTDrop: DropIter with type t = key * D.t = RightProd(struct type t = key end)(D)
    module PairsBase = struct let base = ArrayOfSelf.base () + ArrayOfSelf.nb_state () end
    module Pairs : DropIter with type t = D.t pairs = DropIterArray(PairsBase)(KeyTDrop)

    (* we don't count the number of states of D *)
    let nb_state () = 1 + ArrayOfSelf.nb_state () + Pairs.nb_state ()

    let rec drop_iter t k =
      match t with
      | Leaf pairs -> Pairs.drop_iter pairs k
      | Node (pairs, _) -> begin
        let pairs = Sys.opaque_identity pairs in
        let k = Sys.opaque_identity k in
        let t =  Sys.opaque_identity t in
        let repr = t |> Obj.repr in
        Obj.repr k |> Obj.set_field repr 0;
        Obj.set_tag repr (base ());
        (* build an Aux (k, _) *)
        Pairs.drop_iter pairs (Obj.obj repr)
      end
    and invoke k =
      let repr = Sys.opaque_identity k |> Obj.repr in
      let tag = Obj.tag repr in
      if Obj.is_int repr then begin
        (* k = Empty *)
        ()
      end else if base () <= tag && tag < base () + 1 then begin
        (* k = Aux (k, x) *)
        let k = Obj.field repr 0  |> Obj.obj in
        let x = Obj.field repr 1 |> Obj.obj in
        ArrayOfSelf.drop_iter x k
      end else if
        ArrayOfSelf.base () <= tag && tag < ArrayOfSelf.base () + ArrayOfSelf.nb_state ()
      then begin
        ArrayOfSelf.invoke k
      end else if
        Pairs.base () <= tag && tag < Pairs.base () + Pairs.nb_state ()
      then begin
        Pairs.invoke k
      end else if
        D.base () <= tag && tag < D.base () + D.nb_state ()
      then begin
        D.invoke k
      end else begin
        failwith ("Tag out of bounds " ^ string_of_int tag)
      end
  end
  include Impl
end

let drop_btree : type a. (a -> unit) -> a btree -> unit = fun drop t ->
  let module M = struct type t = a let drop = drop type 'a container = 'a btree end in
  let module D = DefaultDropIterContainer(M)(DropIterBtree) in
  let module Drop = DropIterToDrop(D) in
  Drop.drop t

let drop_int_btree t = drop_btree (fun x -> print_int x; print_newline ()) t

type nonregular = A of int | B of nonregular btree

module rec DropIterNonRegular : DropIter with type t = nonregular = struct
  type t = nonregular
  module D = DropIterBtree(struct let base = 0 end)(DropIterNonRegular)
  let nb_state () = D.nb_state ()
  let base () = 0
  let rec drop_iter x k = match x with
  | A x -> print_int x; print_newline (); invoke k
  | B t -> D.drop_iter t k
  and invoke k = D.invoke k
end
