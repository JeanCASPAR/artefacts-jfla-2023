open Drop_iter;;

(* B-tree and its efficient destructor *)
type key = int

type 'a pairs = (key * 'a) array

type 'a btree =
| Leaf of 'a pairs
| Node of 'a pairs * 'a btree array

(* efficient destructor for the B-tree, parameterized
   by the base value and the efficient destructor for 'a *)
module DropIterBtree :
  functor (Self : Base) (D : DropIter)
    -> DropIter with type t = D.t btree

(* takes in parameter the (non-efficient) destructor of the 'a parameter *)
val drop_btree : ('a -> unit) -> 'a btree -> unit
val drop_int_btree : int btree -> unit

(* efficient destructor for the type which broke the first
  interface *)
type nonregular = A of int | B of nonregular btree
module rec DropIterNonRegular : DropIter with type t = nonregular
