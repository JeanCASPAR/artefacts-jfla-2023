(* interface for an efficient destructor in OCaml *)
module type DropIter =
  sig
    type t
    val nb_state : unit -> int
    val base : unit -> int
    val drop_iter : t -> 'a -> unit
    val invoke : 'a -> unit
  end

(* The first tag used by the continuation *)
module type Base = sig val base : int end

(* Produces a DropIter implementation for a type t, provided a drop function
   and the invoke function to call when the drop call has ended, in order to
   dispatch the continuation *)
module DefaultDropIter :
  functor
    (M : sig type t val drop : t -> unit val invoke : 'a -> unit end)
    -> DropIter with type t = M.t

module DropIterToDrop(D: DropIter) : sig
  val drop : D.t -> unit
end

(* Builds a DropIter implementation for a `t container`, provided a drop function for t and
   a functor which takes a DropIter implementation D and produces a DropIter implementation
   for D.t container *)
module DefaultDropIterContainer :
  functor (M : sig type t val drop : t -> unit type 'a container end)
    (F : functor (Self : Base)
           (D : DropIter with type t = M.t)
           -> DropIter with type t = M.t M.container
    )
    -> DropIter with type t = M.t M.container
