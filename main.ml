open Drop_iter;;
open Drop_iter_btree;;

let () = print_endline "===== DropIter interface for btree ====="
let t =
  (* build an int B-tree where all keys are zero and values start from accu *)
  (* n : height, m : size of pairs, k : number of children *)
  let rec make_btree n m k accu =
    let pairs = Array.init m (fun _ ->
      let value = !accu
      in incr accu;
      (0, value)
    ) in if n = 0 then
      Leaf pairs
    else
      let children = Array.init k (fun _ ->
        make_btree (n-1) m k accu
      ) in Node (pairs, children)
  in make_btree 3 3 3 (ref 0)
let () = drop_int_btree t

let () = print_endline "===== DropIter interface for nonregular ====="
let nonregular = B (
  Node ([| (0, A 0); (1, A 1) |], [| Node (
    [| (2, A 2) |],
    [| Leaf [| (3, A 3); (4, A 4); (5, A 5) |] |]
  )|])
)
let () =
  let module Drop = Drop_iter.DropIterToDrop(DropIterNonRegular)
  in Drop.drop nonregular

open Drop_iter_mutu_rec;;

let () = print_endline "===== DropIter interface for mutually recursive types ====="
let example_value =
  let b3 = YTreeX (XLeaf, 0) in
  let a1 = XTree (b3, 1, YLeaf) in
  let b1 = YTreeX (a1, 2) in
  let a2 = XTree (YLeaf, 4, YTreeY (YLeaf, 5)) in
  let b2 = YTreeY (YTreeX (a2, 6), 7) in
  XTree (b1, 3, b2)
let () =
  let module Drop = Drop_iter.DropIterToDrop(DropIterXTree)
  in Drop.drop example_value
