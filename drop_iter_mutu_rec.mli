open Drop_iter;;

(* Mutually recursive types and their efficient destructor *)
type xtree =
| XLeaf
| XTree of ytree * int * ytree
and ytree =
| YLeaf
| YTreeX of xtree * int
| YTreeY of ytree * int

module DropIterXTree : DropIter with type t = xtree
module DropIterYTree : DropIter with type t = ytree