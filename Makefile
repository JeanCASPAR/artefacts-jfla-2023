all: cpp ocaml

cpp: btree.cpp btree.hpp
	clang++ btree.cpp -o btree.exe -Wpedantic -g -fsanitize=address -std=c++20 -fno-omit-frame-pointer

ocaml: *.ml *.mli
	ocamlopt drop_iter.mli
	ocamlopt drop_iter_array.mli
	ocamlopt drop_iter_btree.mli
	ocamlopt drop_iter_mutu_rec.mli
	ocamlopt -c drop_iter.ml
	ocamlopt -c drop_iter_array.ml
	ocamlopt -c drop_iter_btree.ml
	ocamlopt -c drop_iter_mutu_rec.ml
	ocamlopt drop_iter.cmx drop_iter_array.cmx drop_iter_btree.cmx drop_iter_mutu_rec.cmx main.ml -o main.exe
.PHONY: clean

clean:
	rm *.exe *.cmi *.cmx *.o
