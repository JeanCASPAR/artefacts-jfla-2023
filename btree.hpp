#include <vector>
#include <memory>
#include <cstdint>

union CPSPointer {
    uintptr_t flag;
    void* ptr;

    const static uintptr_t HIGH_BIT_MASK = UINTPTR_MAX - ((1ULL << 48) - 1);

    // set the first three bits and the higher bits to 0
    inline constexpr void nullify() {
        this->flag &= ~0b111;
        this->flag &= ~HIGH_BIT_MASK;
    }

    // reset to a valid pointer
    inline constexpr void reset() {
        this->nullify();
        if ((this->flag >> 47) & 1) {
            // we must sign-extend
            this->flag |= HIGH_BIT_MASK;
        }
    }

    // set the tag to I
    template<unsigned int I>
    constexpr void set() {
        static_assert(I < (1 << 19));
        this->nullify();
        this->flag |= I & 0b111;
        this->flag |= static_cast<unsigned long long>(I >> 3) << 48;
    }

    // return the tag
    inline constexpr unsigned int get() {
        uintptr_t low_bits = this->flag & 0b111;
        uintptr_t high_bits = (this->flag >> 48) << 3;
        return low_bits | high_bits;
    }
};

// I don't know enough C++ to provide a nice generic interface like in OCaml,
// therefore I will only provide a destructor for a BTree that holds an int pointer,
// and I will print it when I destruct it, but the translation for other types
// is straightforward.
struct BTree {
    std::unique_ptr<int> data;
    std::vector<BTree> nodes;

    BTree(std::unique_ptr<int> data, std::vector<BTree> nodes):
        data{std::move(data)}, nodes{std::move(nodes)} {}

    BTree clone();

    BTree(BTree&&) = default;
    BTree& operator=(BTree&&) = default;
    // destructs the data before the children, and destruct the children
    // in reverse order
    ~BTree();
};

// vector utilities
template<typename T>
T* pop(std::vector<T>&);
template<typename T>
T** capacity_field(std::vector<T>& v);
template<typename T>
T** length_field(std::vector<T>& v);
template<typename T>
T** alloc_field(std::vector<T>& v);

// builds a BTree of height n, where each nodes except the leaf have w children,
// and each nodes holds a different number
BTree builder(unsigned int w, unsigned int n);
void rename(BTree&, unsigned int& i);

// the pointer must be nonnull
// does not free the memory of the pointer
void drop_iter_btree(BTree*, CPSPointer);
void invoke_btree(CPSPointer);

template<typename U, unsigned int OFFSET>
void drop_field_btree(U*, CPSPointer);
template<typename U, unsigned int OFFSET>
void invoke_field_btree(CPSPointer);
CPSPointer retrieve(BTree*);

template<typename U, unsigned int OFFSET>
void drop_field_vector(U*, CPSPointer);
template<typename U, unsigned int OFFSET>
void invoke_field_vector(CPSPointer);
template<typename T>
CPSPointer retrieve_vector(std::vector<T>*);
