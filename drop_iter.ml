module type DropIter = sig
  type t
  val nb_state : unit -> int
  val base : unit -> int
  val drop_iter : t -> 'a -> unit
  val invoke : 'a -> unit
end

module type Base = sig
  val base : int
end

module DefaultDropIter(M : sig
  type t
  val drop : t -> unit
  val invoke : 'a -> unit
end) : DropIter with type t = M.t = struct
  type t = M.t
  let nb_state () = 0
  let base () = 0
  let drop_iter t k = M.drop t; M.invoke k
  let invoke k = M.invoke k
end

module DropIterToDrop(D: DropIter) = struct
  let drop x = D.drop_iter x (Obj.magic 0)
end

module DefaultDropIterContainer (M : sig
  type t
  val drop : t -> unit
  type 'a container
end) (F : functor (Self : Base)
           (D : DropIter with type t = M.t)
           -> DropIter with type t = M.t M.container
    ) : DropIter with type t = M.t M.container
= struct
  module rec Impl : DropIter
  with type t = M.t M.container = F(Self)(D)
  and Self: Base = struct
    let base = 0
  end and D : DropIter with type t = M.t = struct
    type t = M.t
    let nb_state () = 0
    let base () = 0
    let drop_iter t k = M.drop t; Impl.invoke k
    let invoke k = Impl.invoke k
  end

  include Impl
end
