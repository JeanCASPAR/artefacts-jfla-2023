#include <iostream>
#include <memory>
#include <cstring>
#include <cstdint>
#include <vector>

#include "btree.hpp"

// returns a pointer to the last element of v
// and decrements its length
template<typename T>
T* pop(std::vector<T>& v) {
    T* ptr = &v.back();
    T** len = length_field<BTree>(v);
    *len -= 1;
    return ptr;
}

// pointer to the capacity field of v
template<typename T>
T** capacity_field(std::vector<T>& v) {
    T** capacity = reinterpret_cast<T**>(&v) + 2;
    return capacity;
}

// pointer to the length field of v
template<typename T>
T** length_field(std::vector<T>& v) {
    T** length = reinterpret_cast<T**>(&v) + 1;
    return length;
}

// pointer to the field that stores the allocation of v
template<typename T>
T** alloc_field(std::vector<T>& v) {
    T** alloc = reinterpret_cast<T**>(&v);
    return alloc;
}

void rename(BTree& t, unsigned int& i) {
    if (t.data) {
        *(t.data) = i++;
    }
    for (
        auto it = t.nodes.rbegin();
        it != t.nodes.rend();
        it++
    ) {
        rename(*it, i);
    }
}

BTree builder(unsigned int w, unsigned int n) {
    BTree t { std::make_unique<int>(0), {} };
    if (n > 0) {
        for (unsigned int i = 0; i < w; i++) {
            t.nodes.emplace_back(BTree { std::make_unique<int>(0), {} });
        }
    }
    for (unsigned int i = 0; i < n - 1; i++) {
        BTree clone = t.clone();
        for (auto& child : t.nodes) {
            child = clone.clone();
        }
    }
    unsigned int i = 0;
    rename(t, i);
    return t;
}

void drop_iter_btree(BTree* t, CPSPointer k) {
    return drop_field_btree<BTree, 0>(t, k);
}

void invoke_btree(CPSPointer k) {
    return invoke_field_btree<BTree, 0>(k);
}

template<typename U, unsigned int OFFSET>
void drop_field_btree(U* u, CPSPointer k) {
    BTree* t = reinterpret_cast<BTree*>(reinterpret_cast<char*>(u) + OFFSET);
    // drop data
    if (t->data) {
        std::cout << "Drop " << *t->data << std::endl;
        t->data = nullptr;
    }
    // drop nodes
    return drop_field_vector<U, OFFSET + offsetof(BTree, nodes)>(u, k);
}

// never called
template<typename U, unsigned int OFFSET>
void invoke_field_btree(CPSPointer) {}

CPSPointer retrieve_btree(BTree* t) {
    return retrieve_vector<BTree>(&t->nodes);
}

template<typename U, unsigned int OFFSET>
void drop_field_vector(U* u, CPSPointer k) {
    std::vector<BTree>* v = reinterpret_cast<std::vector<BTree>*>(reinterpret_cast<char*>(u) + OFFSET);
    if (v->empty()) {
        v->~vector();
        return invoke_field_vector<U, OFFSET>(k);
    }
    if (v->capacity() == v->size()) {
        // we replace the capacity pointer by the continuation pointer
        CPSPointer* ptr = reinterpret_cast<CPSPointer*>(capacity_field<BTree>(*v));
        *ptr = k;
        BTree* new_t = pop<BTree>(*v);
        CPSPointer new_k = { .ptr = u };
        new_k.set<1>();
        return drop_iter_btree(new_t, new_k);
    } else {
        BTree* ptr = &v->front();
        ptr += v->capacity() - 1;
        // sizeof(BTree) > sizeof(CPSPointer) therefore
        // we can write k here, right at the end of the allocation
        CPSPointer* store_k = reinterpret_cast<CPSPointer*>(ptr);
        *store_k = k;
        BTree* new_t = pop<BTree>(*v);
        CPSPointer new_k = { .ptr = u };
        uintptr_t* inner_state = reinterpret_cast<uintptr_t*>(alloc_field<BTree>(*v));
        // we store a tag inside the pointer to the allocation
        *inner_state |= 1;
        new_k.set<1>();
        return drop_iter_btree(new_t, new_k);
    }
}

template<typename U, unsigned int OFFSET>
void invoke_field_vector(CPSPointer k) {
    switch (k.get()) {
        case 0:
            return;
        case 1: {
            k.reset();
            U* u = reinterpret_cast<U*>(k.ptr);
            std::vector<BTree>* v = reinterpret_cast<std::vector<BTree>*>(reinterpret_cast<char*>(u) + OFFSET);
            CPSPointer new_k = retrieve_vector<BTree>(v);
            return drop_field_vector<BTree, OFFSET>(u, new_k);
        }
    }
}

template<typename T>
CPSPointer retrieve_vector(std::vector<T>* v) {
    uintptr_t* inner_state = reinterpret_cast<uintptr_t*>(alloc_field<BTree>(*v));
    if ((*inner_state) & 1) {
        *inner_state ^= 1;
        // capacity > size + 1
        BTree* ptr = &v->front() + v->capacity() - 1;
        CPSPointer* store_k = reinterpret_cast<CPSPointer*>(ptr);
        CPSPointer new_k = *store_k;
        return new_k;
    } else {
        // we retrieve the continuation
        BTree** capacity = capacity_field<BTree>(*v);
        CPSPointer new_k = { .ptr = *capacity };
        BTree* length = *length_field<BTree>(*v);
        // we know that capacity = size + 1
        *capacity = length + 1;
        // now size < capacity, and v is restored to a valid state
        return new_k;
    }
}

BTree BTree::clone() {
    BTree t = { nullptr, {} };
    if (this->data) {
        t.data = std::make_unique<int>(*(this->data));
    }
    t.nodes.reserve(this->nodes.size());
    for (unsigned int i = 0; i < this->nodes.size(); i++) {
        t.nodes.emplace_back(this->nodes[i].clone());
    }
    return t;
}

BTree::~BTree() {
    if (this->data) {
        std::cout << "Drop " << *(this->data) << std::endl;
        this->data = nullptr;
    }

    while(!(this->nodes.empty())) {
        BTree* t = pop<BTree>(this->nodes);
        drop_iter_btree(t, { .ptr = nullptr });
    }
}

// a vector consists of three pointer:
// one to the start of the allocation,
// one past the last element (the length)
// and one past the end of the allocation (the capacity)
// 
// we use two tags:
// tag 0 represents the empty continuation
// tag 1 represents a partially dropped vector
// we store an additional bit of information inside
// the lowest bit of the pointer to the allocation of the vector
// 0 represents a partially dropped vector with size = capacity,
// we store the continuation inside the capacity field
// 1 represents a partially dropped vector with size < capacity,
// we store the continuation right before the end of the allocation
int main() {
    BTree t = builder(3, 3);
    std::cout << "Start dropping" << std::endl;
}
